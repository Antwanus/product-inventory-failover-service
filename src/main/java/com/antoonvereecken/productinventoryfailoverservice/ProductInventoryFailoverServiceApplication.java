package com.antoonvereecken.productinventoryfailoverservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductInventoryFailoverServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductInventoryFailoverServiceApplication.class, args);
	}

}
