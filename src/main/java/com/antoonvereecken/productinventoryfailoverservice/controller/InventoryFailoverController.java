package com.antoonvereecken.productinventoryfailoverservice.controller;

import com.antoonvereecken.productinventoryfailoverservice.model.ProductInventoryDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.UUID;

@RestController
public class InventoryFailoverController {

    @GetMapping(path = "/inventory-failover")
    public ResponseEntity<ProductInventoryDto> respondWithFailover() {
        return ResponseEntity.ok(
                ProductInventoryDto.builder()
                        .inventoryId(UUID.randomUUID())
                        .productId(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                        .currentStock(1337)
                        .createdDate(OffsetDateTime.now())
                        .lastModifiedDate(OffsetDateTime.now())
                        .build()
        );
    }

}
